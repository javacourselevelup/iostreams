package com.anadea.io;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Employee implements Serializable {

    //private static final long serialVersionUID = 42L;

    private String firstName;

    private String lastName;

    private Integer age;

    transient private String nonSerializable;

    public Employee() {
        this("", "", 0, "");
    }

    public Employee(String firstName, String lastName, Integer age, String nonSerializable) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.nonSerializable = nonSerializable;

        sayHello();
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + ", " + age + ", " + nonSerializable;
    }

    public void sayHello() {
        System.out.println("Hello, my name is " + firstName + " " + lastName);
    }

//    private void writeObject(ObjectOutputStream out) throws IOException {
//        out.defaultWriteObject();
//    }
//
//    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
//        in.defaultReadObject();
//        sayHello();
//    }
}
