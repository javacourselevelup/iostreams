package com.anadea.io;

import java.io.*;
import java.util.Random;

public class SeekDataTask {

    public static void main(String[] args) {
        SeekDataTask sdt = new SeekDataTask();
//        sdt.generateFile();
//        sdt.readSecret();
    }

    private void generateFile() {
        try (FileOutputStream fos = new FileOutputStream("seek_me.dat");
             BufferedOutputStream bos = new BufferedOutputStream(fos);
             DataOutputStream dos = new DataOutputStream(bos)) {
            byte[] secret = new byte[] {71, 114, 101, 97, 116, 32, 106, 111, 98, 33};
            dos.writeInt(100);
            dos.writeInt(secret.length);

            Random rnd = new Random();

            for (int i = 0; i < 92; i++) {
                dos.write(rnd.nextInt());
            }

            dos.write(secret);

            for (int i = 0; i < 100; i++) {
                dos.write(rnd.nextInt());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void readSecret() {
        try (FileInputStream fis = new FileInputStream("seek_me.dat");
             BufferedInputStream bis = new BufferedInputStream(fis);
             DataInputStream dis = new DataInputStream(bis)) {
            int shift = dis.readInt();
            int length = dis.readInt();

            dis.skipBytes(shift - 8);
            byte[] secretBytes = new byte[length];
            dis.read(secretBytes);
            String secret = new String(secretBytes);
            System.out.println(secret);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
