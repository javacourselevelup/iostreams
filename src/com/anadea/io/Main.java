package com.anadea.io;

import java.io.*;
import java.util.Locale;

public class Main {

    public static void main(String[] args) throws Exception {
    }

    // Базовые потоки ввода/вывода. Оперируют байтами.
    private void basicStreams() throws Exception {
        InputStream is = new FileInputStream("data.txt");
        is.read(); // Читает 1 байт
        byte[] buffer = new byte[100];

        // Читает максимум buffer.length байт и помещает их в массив buffer.
        // Возвращает количество фактически прочитанных байт.
        is.read(buffer);

        // Помечает текущую позицию в потоке.
        // Вернуться к этой позиции можно при помощи is.reset()
        // После прочтения N байт (задано параметром) стирает метку
        is.mark(100);
        is.read();
        is.read();
        is.reset();

        // Возвращает примерное количество доступных байт данных для чтения
        is.available();

        // Пропускает следующие N байт данных
        is.skip(30);

        // Закрывает поток
        is.close();

        OutputStream os = new FileOutputStream("data.txt");

        // Записывает 1 байт
        os.write(1);

        // Записывает данные из массива в поток
        os.write(buffer);

        // Записывает в поток 10 байт, начиная с 5го
        os.write(buffer, 5, 10);

        // Принудительно записывает данные в поток
        // Полезно с буферизированными потоками
        os.flush();

        // Делает flush и закрывает поток
        os.close();

    }

    // Буферизирует данные при чтении.
    // Полезно при чтении/записи данных из файла, передаче по сети и т.п.
    public void bufferedStreams() throws Exception {
        InputStream is = new FileInputStream("data.txt");
        OutputStream os = new FileOutputStream("data.txt");

        BufferedInputStream bis = new BufferedInputStream(is, 512);
        bis.close();

        BufferedOutputStream bos = new BufferedOutputStream(os, 512);
        bos.close();
    }

    // Создает поток из массива байт
    public void byteArrayStreams() throws Exception {
        OutputStream os = new FileOutputStream("data.txt");

        ByteArrayInputStream bais = new ByteArrayInputStream(new byte[] {1, 2, 3, 4, 5});
        bais.close();

        ByteArrayOutputStream baos = new ByteArrayOutputStream(100);
        baos.size();
        baos.toByteArray();
        baos.toString();
        baos.toString("UTF-8");
        baos.writeTo(os);
        baos.close();
    }

    // Позволяет читать/писать различные типы данных встроенные в Java
    public void dataStreams() throws Exception {
        InputStream is = new FileInputStream("data.txt");
        OutputStream os = new FileOutputStream("data.txt");

        DataInputStream dis = new DataInputStream(is);
        dis.readBoolean();
        dis.readInt();
        dis.readUTF();

        byte[] buffer = new byte[100];
        // Читает buffer.length байт
        dis.readFully(buffer);
        // Работает аналогично InputStream.skip(n)
        dis.skipBytes(10);
        dis.close();

        DataOutputStream dos = new DataOutputStream(os);
        // Возвращает количество записанных байт
        dos.size();
        dos.close();
    }

    private void fileStreams() throws Exception {
        String helloStreams = "Hello, IO Streams";

        FileOutputStream outputStream = new FileOutputStream("data.txt");
        byte[] bytes = helloStreams.getBytes();
        outputStream.write(bytes);
        outputStream.close();

        FileInputStream inputStream = new FileInputStream("data.txt");
        byte[] fileData = new byte[inputStream.available()];
        inputStream.read(fileData);
        inputStream.close();
        String result = new String(fileData);
        System.out.println(result);
    }

    // Все что будет записано в PipedOutputStream будет прочитано из PipedInputStream
    // Удобно использовать для обмена данными между потоками (Thread) в многопоточном приложении
    private void pipedStreams() throws Exception {
        PipedOutputStream pos = new PipedOutputStream();

        PipedInputStream pis = new PipedInputStream(pos);
        pis.connect(pos);

        pos.close();
        pis.close();
    }

    // Позволяет поместить данные обратно в поток для повторного чтения.
    // Полезно при парсинге данных.
    private void pushbackStreams() throws Exception {
        InputStream is = new FileInputStream("data.txt");

        PushbackInputStream pbis = new PushbackInputStream(is);
        // Помещает байт в начало потока
        pbis.unread(1);
        byte[] buffer = new byte[100];
        // Помещает массива байт в начало потока
        pbis.unread(buffer);

        // Помещает 10 байт, начиная с 5го из массива начало потока
        pbis.unread(buffer, 5, 10);
        pbis.close();
    }

    // Склеивает два или более входных потоков друг за другом.
    private void sequenceStream() throws Exception {
        FileInputStream file1 = new FileInputStream("file_1.txt");
        FileInputStream file2 = new FileInputStream("file_2.txt");
        SequenceInputStream sis = new SequenceInputStream(file1, file2);
        sis.close();
    }

    // Позволяет читать/писать сериализованные данные
    public static void objectStreams() throws Exception {
        Employee employee1 = new Employee("John", "Snow", 25, "abra");
        Employee employee2 = new Employee("Chuck", "Norris", 42, "kadabra");

        // Создаем ObjectOutputStream для вывода в файл
        FileOutputStream fos = new FileOutputStream("employees.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        // Пишем данные
        oos.writeObject(employee1);
        oos.writeObject(employee2);
        oos.flush(); // Перед close необязательно
        oos.close();

        // Создаем ObjectInputStream для чтения из файла
        ObjectInputStream ois =
                new ObjectInputStream(new FileInputStream("employees.txt"));

        System.out.println(ois.readObject());
        System.out.println(ois.readObject());

        // Закрываем входной поток
        ois.close();
    }

    // Позволяет удобно читать/писать текстовые данные
    // Работает аналогично Input/Output Stream, но работает не с byte, а с char
    private void readerWriter() throws Exception {
        InputStream is = new FileInputStream("data.txt");
        OutputStream os = new FileOutputStream("data.txt");

        Reader reader = new InputStreamReader(is);
        reader.close();

        Writer writer = new OutputStreamWriter(os);
        writer.close();

    }

    // Позволяет делать форматированный вывод
    // Не бросает IOException
    private void printStream() throws Exception {
        PrintStream ps = new PrintStream(System.out);

        // Работает также как и print
        ps.append("A string\n")
        .append("3645 in HEX is: ")
        .format(Locale.ENGLISH, "%1H\n", 3645);

        // Делает flush и возвращает true, если произошла ошибка ввода/вывода
        ps.checkError();

        ps.close();
    }

}
