package com.anadea.io;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class LetterCountTask {

    public static void main(String[] args) {
        LetterCountTask lct = new LetterCountTask();
        System.out.println(lct.countLetters('e'));
    }

    private int countLetters(char c) {
        int count = 0;

        try (FileInputStream fis = new FileInputStream("xanadu.txt");
             Reader r = new InputStreamReader(fis);
             BufferedReader br = new BufferedReader(r)) {

            while (br.ready()) {
                if (br.read() == c) count++;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return count;
    }

}
